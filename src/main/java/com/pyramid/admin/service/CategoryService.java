package com.pyramid.admin.service;

import com.pyramid.admin.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class CategoryService {
    @Autowired
    private CategoryRepository cr;

    public void addCategory(Category c){
        cr.save(c);
    }
    public List<Category> getCategories(){
        List<Category> list = new ArrayList<>();
        cr.findAll().forEach(list::add);
        return list;
    }
}
